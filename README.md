# 博客前端和后台--Vue

## 介绍
应用模板，搭建个人博客系统。前端和后台使用Vue框架，后端([blog-server](https://gitee.com/BeastAndBeauty/blog-server))使用SSM框架。

## 项目演示
前端项目`blog-client`地址：[www.paopao.asia](http://www.paopao.asia)

### 前端效果图
![输入图片说明](https://images.gitee.com/uploads/images/2020/0505/230609_7943d875_5099817.png "在这里输入图片标题")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0505/230801_77e16b27_5099817.png "在这里输入图片标题")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0505/230226_3d500bff_5099817.png)

后台项目`blog-admin`地址：[http://paopao.asia:8888/blog-admin/](http://paopao.asia:8888/blog-admin/)

### 后端效果图
![输入图片说明](https://images.gitee.com/uploads/images/2020/0505/231344_f06a728f_5099817.png "在这里输入图片标题")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0505/231415_1f5e4cdb_5099817.png "在这里输入图片标题")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0505/231450_6385f293_5099817.png "在这里输入图片标题")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0505/231604_c667f5ac_5099817.png "在这里输入图片标题")

## 使用说明

1. 博客前端项目

   ```
   # 切换到博客前端项目目录
   cd blog-client
   
   # 安装依赖
   npm install
   
   # 本地环境调试
   npm run dev
   ```

2.  博客后台项目

    ```
    # 切换到博客后台项目目录
    cd blog-admin
    
    # 安装依赖
    npm install
    
    # 本地环境调试
    # 若运行下面命令出现此报错 Error: Cannot find module 'node-sass'
    # 则先运行 cnpm install node-sass@latest
    # 再运行下面命令
    npm run dev
    ```
## Vue模板地址

1.  前端项目：[https://github.com/shimh-develop/blog-vue-springboot](https://github.com/shimh-develop/blog-vue-springboot)
2.  后台项目：[https://github.com/antbaobao/AntVueBlogAdmin]( https://github.com/antbaobao/AntVueBlogAdmin )
