'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
    NODE_ENV : '"development"',
    GATEWAY : '"http://localhost:8888"',
    STATIC_DOMAIN : '"http://blog.paopao.asia"'
})
