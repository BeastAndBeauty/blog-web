import _request from '@/utils/request'

export function fetchArticle(params) {
  return _request.get('/article', params)
}

export function getArticleDetail(id) {
  return _request.get(`/article/${id}`)
}

export function createArticle(data) {
  return _request.post('/admin/article', data)
}

export function updateArticle(data) {
  return _request.put('/admin/article', data)
}

export function deleteArticle(id) {
  return _request.delete(`/admin/article/${id}`)
}
