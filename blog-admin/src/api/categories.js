import _request from '@/utils/request'

export function create(category) {
  return _request.post('/admin/category', category)
}

export function getCategories() {
  return _request.get('/category')
}

export function deleteCategory(id) {
  return _request.delete(`/admin/category/${id}`)
}

export function update(category) {
  return _request.put(`/admin/category`, category)
}
