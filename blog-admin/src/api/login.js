import _request from '@/utils/request'

export function login(account, password) {
    return _request.post('/user/admin/login', {account, password})
}

export function getInfo() {
    return _request.get('/user')
}

export function logout() {
    return request({
        url: '/user/logout',
        method: 'post',
    })
}
