import _request from '@/utils/request'

export function create(tag) {
  return _request.post('/admin/tag', tag)
}

export function getList() {
  return _request.get('/tag')
}

export function deleteTag(id) {
  return _request.delete(`/admin/tag/${id}`)
}

export function updateTag(tag) {
  return _request.put(`/admin/tag`, tag)
}
