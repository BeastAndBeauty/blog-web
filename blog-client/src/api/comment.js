import request from '@/request'


export function getCommentsByArticle(id) {
  return request({
    url: '/comment',
    method: 'get',
    params:{
      id:id
    }
  })
}

export function publishComment(comment) {
  return request({
    url: '/comment',
    method: 'post',
    data: comment
  })
}

