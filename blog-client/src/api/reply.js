import request from '@/request'

export function publishReply(reply) {
    return request({
      url: '/reply',
      method: 'post',
      data: reply
    })
  }