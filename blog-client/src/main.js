
import Vue from 'vue'
import App from './App'

import router from './router'
import store from './store'

import lodash from 'lodash'

import ElementUI from 'element-ui'
import '@/assets/theme/index.css'

import '@/assets/icon/iconfont.css'
import mavonEditor from 'mavon-editor'
import 'mavon-editor/dist/css/index.css'

import { formatTime } from "./utils/time";
import axios from 'axios'
//axios请求默认是不携带cookie的，让了让其带上cookie
axios.defaults.withCredentials = true
Vue.config.productionTip = false

Vue.use(ElementUI)
Vue.use(mavonEditor)
Object.defineProperty(Vue.prototype, '$_', { value: lodash })


Vue.directive('title', function (el, binding) {
  document.title = el.dataset.title
})
// 格式话时间
Vue.filter('format', formatTime)

new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
